#-------------------------------------------------------------------------------
# libcoreaws3/configure.ac
#-------------------------------------------------------------------------------
# Copyright 2012 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

AC_INIT(
    [coreaws],
    [3.0.0],
    [contact@dowdandassociates.com])
AM_INIT_AUTOMAKE([-Wall -Werror foreign])
m4_ifdef([AM_PROG_AR], [AM_PROG_AR])
AC_PROG_CXX
AC_PROG_LIBTOOL
AC_LANG(C++)
AC_ARG_WITH(
    [curl],
    AC_HELP_STRING([--with-curl=PATH], [where libcurl is installed]),
    [if test x$with_curl != x_/usr; then
        LDFLAGS="-L$with_curl $LDFLAGS"
        CPPFLAGS="-I$with_curl $CPPFLAGS"
    fi]
    )
AC_CHECK_LIB(
    [curl],
    [main],
    [CURLLIB=-lcurl],
    [AC_MSG_ERROR([unable to link with libcurl])]
    )
AC_SUBST([CURLLIB])
AC_ARG_WITH(
    [openssl],
    AC_HELP_STRING([--with-openssl=PATH], [where openssl is installed]),
    [if test x$with_openssl != x_/usr; then
        LDFLAGS="-L$with_openssl $LDFLAGS"
        CPPFLAGS="-I$with_openssl $CPPFLAGS"
    fi]
    )
AC_CHECK_LIB(
    [crypto],
    [main],
    [CRYPTOLIB=-lcrypto],
    [AC_MSG_ERROR([unable to link with libcrypto])]
    )
AC_SUBST([CRYPTOLIB])
AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_HEADERS([config.h])
AC_CHECK_HEADERS([algorithm],,[AC_MSG_ERROR([Cannot find algorithm])],)
AC_CHECK_HEADERS([cctype],,[AC_MSG_ERROR([Cannot find cctype])],)
AC_CHECK_HEADERS([cstddef],,[AC_MSG_ERROR([Cannot find cstddef])],)
AC_CHECK_HEADERS([cstdio],,[AC_MSG_ERROR([Cannot find cstdio])],)
AC_CHECK_HEADERS([cstdlib],,[AC_MSG_ERROR([Cannot find cstdlib])],)
AC_CHECK_HEADERS([cstring],,[AC_MSG_ERROR([Cannot find cstring])],)
AC_CHECK_HEADERS([ctime],,[AC_MSG_ERROR([Cannot find ctime])],)
AC_CHECK_HEADERS([curl/curl.h],,[AC_MSG_ERROR([Cannot find curl/curl.h])],)
AC_CHECK_HEADERS([fstream],,[AC_MSG_ERROR([Cannot find fstream])],)
AC_CHECK_HEADERS([istream],,[AC_MSG_ERROR([Cannot find istream])],)
AC_CHECK_HEADERS([iostream],,[AC_MSG_ERROR([Cannot find iostream])],)
AC_CHECK_HEADERS([map],,[AC_MSG_ERROR([Cannot find map])],)
AC_CHECK_HEADERS([numeric],,[AC_MSG_ERROR([Cannot find numeric])],)
AC_CHECK_HEADERS([openssl/bio.h],,[AC_MSG_ERROR([Cannot find openssl/bio.h])],)
AC_CHECK_HEADERS([openssl/evp.h],,[AC_MSG_ERROR([Cannot find openssl/evp.h])],)
AC_CHECK_HEADERS([openssl/hmac.h],,[AC_MSG_ERROR([Cannot find openssl/hmac.h])],)
AC_CHECK_HEADERS([ostream],,[AC_MSG_ERROR([Cannot find ostream])],)
AC_CHECK_HEADERS([sstream],,[AC_MSG_ERROR([Cannot find sstream])],)
AC_CHECK_HEADERS([stdexcept],,[AC_MSG_ERROR([Cannot find stdexcept])],)
AC_CHECK_HEADERS([string],,[AC_MSG_ERROR([Cannot find string])],)
AC_CHECK_HEADERS([tr1/memory],,[AC_MSG_ERROR([Cannot find tr1/memory])],)
AC_CHECK_HEADERS([utility],,[AC_MSG_ERROR([Cannot find utility])],)
AC_CONFIG_FILES([Makefile src/Makefile])
AC_OUTPUT

