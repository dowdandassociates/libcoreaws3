/*
 *
 * libcoreaws3/src/S3RequestTest.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include <iostream>
#include <sstream>
#include <string>

#include <curl/curl.h>

#include "common.hpp"

const char* BUCKET = "dowdandassociates-test";
const char* KEY = "test x.txt";

int main(int argc, char** argv)
{
    try
    {
        coreaws3::Connection::init();

        coreaws3::SignerHandle signer(new coreaws3::S3Signer());
        coreaws3::CredentialsProviderHandle credentialsProvider(
                new coreaws3::EnvironmentVariableCredentialsProvider());

        coreaws3::HttpMethod::Type putMethod = coreaws3::HttpMethod::PUT;
        coreaws3::EndpointHandle endpoint(new coreaws3::Endpoint(
                coreaws3::Scheme::https,
                "s3.amazonaws.com",
                coreaws3::Maybe<coreaws3::Port>::nothing()));
        std::stringstream strbuf;
        strbuf << '/' << BUCKET << '/' << KEY;
        std::string resourcePath = strbuf.str();
        coreaws3::ParameterMap parameters;
        coreaws3::HeaderMap headers;
        coreaws3::Maybe<coreaws3::InputStreamHandle>::Type putContent =
                coreaws3::Maybe<coreaws3::InputStreamHandle>::just(
                        coreaws3::StreamUtils::openInputFile("input.txt"));

        coreaws3::RequestHandle putRequest(new coreaws3::Request(
                putMethod,
                endpoint,
                resourcePath,
                parameters,
                headers,
                putContent));

        coreaws3::RequestHandle signedPutRequest =
                (*signer)(credentialsProvider,
                          putRequest,
                          coreaws3::DateTimeUtils::now());

        coreaws3::HttpRequestHandle httpPutRequest =
                coreaws3::HttpRequestFactory::convertRequest(signedPutRequest);

        coreaws3::ResponseHandle putResponse =
                coreaws3::Connection::execute(httpPutRequest);

        if (CURLE_OK != putResponse->returnCode)
        {
            std::cerr << "Error: " << putResponse->errorMessage << " ["
                      << curl_easy_strerror(putResponse->returnCode) << "] ("
                      << putResponse->returnCode << ')' << std::endl;
            return 1;
        }

        coreaws3::OutputStreamHandle putOutputStream = putResponse->output;
        std::tr1::shared_ptr<std::stringstream> putOutput =
                std::tr1::static_pointer_cast<std::stringstream>(
                        putOutputStream);
        std::cout << putOutput->str() << std::endl;

        coreaws3::HttpMethod::Type getMethod = coreaws3::HttpMethod::GET;
    
        coreaws3::Maybe<coreaws3::InputStreamHandle>::Type getContent =
                coreaws3::Maybe<coreaws3::InputStreamHandle>::nothing();

        coreaws3::RequestHandle getRequest(new coreaws3::Request(
                getMethod,
                endpoint,
                resourcePath,
                parameters,
                headers,
                getContent));

        coreaws3::OutputStreamHandle outputFile =
                coreaws3::StreamUtils::openOutputFile("output.txt");

        coreaws3::RequestHandle signedGetRequest =
                (*signer)(credentialsProvider,
                          getRequest,
                          coreaws3::DateTimeUtils::now());

        coreaws3::HttpRequestHandle httpGetRequest =
                coreaws3::HttpRequestFactory::convertRequest(signedGetRequest);

        coreaws3::ResponseHandle getResponse =
                coreaws3::Connection::execute(httpGetRequest, outputFile);


        if (CURLE_OK != getResponse->returnCode)
        {
            std::cerr << "Error: " << getResponse->errorMessage << " ["
                      << curl_easy_strerror(getResponse->returnCode) << "] ("
                      << getResponse->returnCode << ')' << std::endl;
            return 1;
        }

        coreaws3::Connection::cleanup();
        return 0;
    }
    catch (std::exception e)
    {
        std::cerr << "Exception: " << e.what() << std::endl;
        return 1;
    }
    catch (...)
    {
        std::cerr << "Unknown exception" << std::endl;
        return 1;
    }

}

