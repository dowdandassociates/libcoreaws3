#-------------------------------------------------------------------------------
# libcoreaws3/src/Makefile.am
#-------------------------------------------------------------------------------
# Copyright 2012 Dowd and Associates
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

noinst_PROGRAMS = \
	AWS2RequestTest \
	AWS2RequestToCurl \
	AWS3HttpsRequestTest \
	AWS3HttpsRequestToCurl \
	AWS3RequestTest \
	AWS3RequestToCurl \
	AWS4RequestTest \
	AWS4RequestToCurl \
	CloudFrontRequestTest \
	CloudFrontRequestToCurl \
	S3QueryStringRequestToCurl \
	S3RequestTest \
	S3RequestToCurl

AWS2RequestTest_SOURCES = \
	AWS2RequestTest.cpp \
	common.cpp

AWS2RequestTest_LDADD = $(CRYPTOLIB) $(CURLLIB) -lcoreaws3

AWS2RequestToCurl_SOURCES = \
	AWS2RequestToCurl.cpp \
	common.cpp

AWS2RequestToCurl_LDADD = $(CRYPTOLIB) $(CURLLIB) -lcoreaws3

AWS3HttpsRequestTest_SOURCES = \
	AWS3HttpsRequestTest.cpp \
	common.cpp

AWS3HttpsRequestTest_LDADD = $(CRYPTOLIB) $(CURLLIB) -lcoreaws3

AWS3HttpsRequestToCurl_SOURCES = \
	AWS3HttpsRequestToCurl.cpp \
	common.cpp

AWS3HttpsRequestToCurl_LDADD = $(CRYPTOLIB) $(CURLLIB) -lcoreaws3

AWS3RequestTest_SOURCES = \
	AWS3RequestTest.cpp \
	common.cpp

AWS3RequestTest_LDADD = $(CRYPTOLIB) $(CURLLIB) -lcoreaws3

AWS3RequestToCurl_SOURCES = \
	AWS3RequestToCurl.cpp \
	common.cpp

AWS3RequestToCurl_LDADD = $(CRYPTOLIB) $(CURLLIB) -lcoreaws3

AWS4RequestTest_SOURCES = \
	AWS4RequestTest.cpp \
	common.cpp

AWS4RequestTest_LDADD = $(CRYPTOLIB) $(CURLLIB) -lcoreaws3

AWS4RequestToCurl_SOURCES = \
	AWS4RequestToCurl.cpp \
	common.cpp

AWS4RequestToCurl_LDADD = $(CRYPTOLIB) $(CURLLIB) -lcoreaws3

CloudFrontRequestTest_SOURCES = \
	CloudFrontRequestTest.cpp \
	common.cpp

CloudFrontRequestTest_LDADD = $(CRYPTOLIB) $(CURLLIB) -lcoreaws3

CloudFrontRequestToCurl_SOURCES = \
	CloudFrontRequestToCurl.cpp \
	common.cpp

CloudFrontRequestToCurl_LDADD = $(CRYPTOLIB) $(CURLLIB) -lcoreaws3

S3QueryStringRequestToCurl_SOURCES = \
	S3QueryStringRequestToCurl.cpp \
	common.cpp

S3QueryStringRequestToCurl_LDADD = $(CRYPTOLIB) $(CURLLIB) -lcoreaws3

S3RequestTest_SOURCES = \
	S3RequestTest.cpp \
	common.cpp

S3RequestTest_LDADD = $(CRYPTOLIB) $(CURLLIB) -lcoreaws3

S3RequestToCurl_SOURCES = \
	S3RequestToCurl.cpp \
	common.cpp

S3RequestToCurl_LDADD = $(CRYPTOLIB) $(CURLLIB) -lcoreaws3

lib_LTLIBRARIES = libcoreaws3.la

libcoreaws3_la_SOURCES = \
	coreaws3/AWS2Signer.cpp \
	coreaws3/AWS3HttpsSigner.cpp \
	coreaws3/AWS3Signer.cpp \
	coreaws3/AWS4Signer.cpp \
	coreaws3/AWSHostNameUtils.cpp \
	coreaws3/BinaryUtils.cpp \
	coreaws3/CloudFrontSigner.cpp \
	coreaws3/Connection.cpp \
	coreaws3/Credentials.cpp \
	coreaws3/CredentialsProvider.cpp \
	coreaws3/DateTimeUtils.cpp \
	coreaws3/Endpoint.cpp \
	coreaws3/EnvironmentVariableCredentialsProvider.cpp \
	coreaws3/HashUtils.cpp \
	coreaws3/HttpMethod.cpp \
	coreaws3/HttpRequest.cpp \
	coreaws3/HttpRequestFactory.cpp \
	coreaws3/HttpUtils.cpp \
	coreaws3/Request.cpp \
	coreaws3/Response.cpp \
	coreaws3/S3QueryStringSigner.cpp \
	coreaws3/S3Signer.cpp \
	coreaws3/Scheme.cpp \
	coreaws3/Signer.cpp \
	coreaws3/StreamUtils.cpp \
	coreaws3/StringUtils.cpp

libcoreaws3_la_LDFLAGS = \
	$(CRYPTOLIB) $(CURLLIB)

nobase_include_HEADERS = \
	coreaws3/AWS2Signer.hpp \
	coreaws3/AWS3HttpsSigner.hpp \
	coreaws3/AWS3Signer.hpp \
	coreaws3/AWS4Signer.hpp \
	coreaws3/AWSHostNameUtils.hpp \
	coreaws3/BinaryUtils.hpp \
	coreaws3/Buffer.hpp \
	coreaws3/CloudFrontSigner.hpp \
	coreaws3/Connection.hpp \
	coreaws3/Credentials.hpp \
	coreaws3/CredentialsHandle.hpp \
	coreaws3/CredentialsProvider.hpp \
	coreaws3/CredentialsProviderHandle.hpp \
	coreaws3/DateTimeUtils.hpp \
	coreaws3/Endpoint.hpp \
	coreaws3/EndpointHandle.hpp \
	coreaws3/EnvironmentVariableCredentialsProvider.hpp \
	coreaws3/HashUtils.hpp \
	coreaws3/HeaderEntry.hpp \
	coreaws3/HeaderMap.hpp \
	coreaws3/HttpMethod.hpp \
	coreaws3/HttpRequest.hpp \
	coreaws3/HttpRequestFactory.hpp \
	coreaws3/HttpRequestHandle.hpp \
	coreaws3/HttpUtils.hpp \
	coreaws3/InputStreamHandle.hpp \
	coreaws3/Maybe.hpp \
	coreaws3/OutputStreamHandle.hpp \
	coreaws3/ParameterEntry.hpp \
	coreaws3/ParameterMap.hpp \
	coreaws3/Port.hpp \
	coreaws3/Request.hpp \
	coreaws3/RequestHandle.hpp \
	coreaws3/Response.hpp \
	coreaws3/ResponseHandle.hpp \
	coreaws3/S3QueryStringSigner.hpp \
	coreaws3/S3Signer.hpp \
	coreaws3/Scheme.hpp \
	coreaws3/Signer.hpp \
	coreaws3/SignerHandle.hpp \
	coreaws3/StreamUtils.hpp \
	coreaws3/StringUtils.hpp \
	coreaws3/coreaws3.hpp

