/*
 *
 * libcoreaws3/src/coreaws3/Connection.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "Connection.hpp"

#include <cstddef>
#include <cstring>
#include <numeric>
#include <sstream>
#include <stdexcept>

#include "HttpMethod.hpp"
#include "Response.hpp"
#include "StreamUtils.hpp"

namespace coreaws3
{

void Connection::init()
{
    curl_global_init(CURL_GLOBAL_SSL);
}

void Connection::cleanup()
{
    curl_global_cleanup();
}

size_t Connection::read(void* ptr, size_t size, size_t nmemb, void* data)
{
    std::istream* input;
    char buffer[1];
    int cnt;
    int len;

    if (data == NULL)
    {
        throw std::invalid_argument("NULL pointer passed to input callback");
    }

    input = reinterpret_cast<std::istream*>(data);

    for (cnt = 0, len = size * nmemb; cnt < len; ++cnt)
    {
        input->read(buffer, 1);
        if (input->eof() || input->fail())
        {
            break;
        }
        memcpy(((char*)ptr) + cnt, buffer, 1);
    }

    return cnt;
}

size_t Connection::write(void* ptr, size_t size, size_t nmemb, void* data)
{
    std::ostream* output;
    char* buffer;
    int i;
    int len;
    int pos;

    if (data == NULL)
    {
        throw std::invalid_argument("NULL pointer passed to output callback");
    }

    output = reinterpret_cast<std::ostream*>(data);
    buffer = reinterpret_cast<char*>(ptr);

    for (i = 0, pos = 0; i < nmemb; ++i, pos += size)
    {
        output->write(buffer + pos, size);
        if (output->bad())
        {
            break;
        }
    }

    return pos;
}

ResponseHandle Connection::execute(HttpRequestHandle request,
                                   OutputStreamHandle output,
                                   OutputStreamHandle header)
{
    CURL* curl = Connection::initCurl();
    Connection::setVerbose(curl, request->verbose);
    Connection::setTimeout(curl, request->timeout);
    Connection::setURL(curl, request->url);
    Connection::setMethod(curl, request->method);
    if (request->method == HttpMethod::POST)
    {
        Connection::setInputStream(curl, request->content);
        Connection::setPostFieldSize(
                curl,
                StreamUtils::getInputSize(request->content));
    }
    else if (request->method == HttpMethod::PUT)
    {
        Connection::setInputStream(curl, request->content);
        Connection::setInFileSize(
                curl,
                StreamUtils::getInputSize(request->content));
    }
    curl_slist* slist = Connection::setHttpHeaders(curl, request->headers);
    Connection::setHeaderStream(curl, header);
    Connection::setOutputStream(curl, output);
    Connection::setFollowLocation(curl, true);
    char* errorBuffer = Connection::setErrorBuffer(curl);
    CURLcode rc = curl_easy_perform(curl);
    ResponseHandle response(
            new Response(curl, rc, errorBuffer, slist, output, header));

    return response;
}

ResponseHandle Connection::execute(HttpRequestHandle request,
                                   OutputStreamHandle output)
{
    OutputStreamHandle header = StreamUtils::emptyOutputStream();
    return Connection::execute(request, output, header);
}

ResponseHandle Connection::execute(HttpRequestHandle request)
{
    OutputStreamHandle output = StreamUtils::emptyOutputStream();
    return Connection::execute(request, output);
}

CURL* Connection::initCurl()
{
    CURL* curl = curl_easy_init();
    if (NULL == curl)
    {
        throw std::runtime_error("Cannot allocate CURL handle");
    }

    return curl;
}

char* Connection::setErrorBuffer(CURL* curl)
{
    char* errorMessage = new char[CURL_ERROR_SIZE + 1];
    memset(errorMessage, 0, CURL_ERROR_SIZE + 1);
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorMessage);
    if (CURLE_OK != rc)
    {
        curl_easy_cleanup(curl);
        delete [] errorMessage;
        throw std::runtime_error(
                Connection::error("Error setting errob buffer", rc));
    }

    return errorMessage;
}

void Connection::setFollowLocation(CURL* curl, bool follow)
{
    long value = (follow) ? 1 : 0;
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, value);
    if (CURLE_OK != rc)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(
                Connection::error("Error setting follow location", rc));
    }
}

curl_slist* Connection::setHttpHeaders(
            CURL* curl,
            const std::map<std::string, std::string>& headers)
{
    curl_slist* slist = std::accumulate(headers.begin(),
                                        headers.end(),
                                        reinterpret_cast<curl_slist*>(NULL),
                                        Connection::setHttpHeader);

    CURLcode rc = curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
    if (CURLE_OK != rc)
    {
        curl_easy_cleanup(curl);
        curl_slist_free_all(slist);
        throw std::runtime_error(
                Connection::error("Error setting HTTP headers", rc));
    }

    return slist;
}

curl_slist* Connection::setHttpHeader(
            curl_slist* acc,
            const std::pair<const std::string, std::string>& header)
{
    std::string entry = header.first + std::string(": ") + header.second;
    acc = curl_slist_append(acc, entry.c_str());
    return acc;
}

void Connection::setInFileSize(CURL* curl, curl_off_t size)
{
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, size);
    if (CURLE_OK != rc)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(
                Connection::error("Error setting infile size", rc));
    }
}

void Connection::setInputStream(CURL* curl, InputStreamHandle inputStream)
{
    CURLcode rc = curl_easy_setopt(curl,
                                   CURLOPT_READFUNCTION,
                                   Connection::read);
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(
                Connection::error("Error setting read function", rc));
    }

    rc = curl_easy_setopt(curl,
                          CURLOPT_READDATA,
                          reinterpret_cast<void*>(inputStream.get()));
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(
                Connection::error("Error setting read stream", rc));
    }
}

void Connection::setOutputStream(CURL* curl, OutputStreamHandle outputStream)
{
    CURLcode rc = curl_easy_setopt(curl,
                                   CURLOPT_WRITEFUNCTION,
                                   Connection::write);
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(
                Connection::error("Error setting write function", rc));
    }

    rc = curl_easy_setopt(curl,
                          CURLOPT_WRITEDATA,
                          reinterpret_cast<void*>(outputStream.get()));
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(
                Connection::error("Error setting write stream", rc));
    }
}

void Connection::setHeaderStream(CURL* curl, OutputStreamHandle headerStream)
{
    CURLcode rc = curl_easy_setopt(curl,
                                   CURLOPT_HEADERFUNCTION,
                                   Connection::write);
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(
                Connection::error("Error setting header function", rc));
    }

    rc = curl_easy_setopt(curl,
                          CURLOPT_WRITEHEADER,
                          reinterpret_cast<void*>(headerStream.get()));
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(
                Connection::error("Error setting header stream", rc));
    }
}

void Connection::setMethod(CURL* curl, HttpMethod::Type method)
{
    CURLcode rc;

    switch (method)
    {
    case HttpMethod::GET:
        rc = curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
        break;
    case HttpMethod::POST:
        rc = curl_easy_setopt(curl, CURLOPT_POST, 1);
        break;
    case HttpMethod::PUT:
        rc = curl_easy_setopt(curl, CURLOPT_UPLOAD, 1);
        break;
    case HttpMethod::DELETE:
        rc = curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        break;
    case HttpMethod::HEAD:
        rc = curl_easy_setopt(curl, CURLOPT_NOBODY, 1);
        break;
    default:
        throw std::runtime_error("Unknown HTTP method");
    }

    if (CURLE_OK != rc)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(
                Connection::error("Error setting HTTP method", rc));
    }
}

void Connection::setPostFields(CURL* curl, const std::string& postField)
{
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postField.c_str());
    if (CURLE_OK != rc)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(
                Connection::error("Error setting post fields", rc));
    }
}

void Connection::setPostFieldSize(CURL* curl, curl_off_t size)
{
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE_LARGE, size);
    if (CURLE_OK != rc)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(
                Connection::error("Error setting post fields", rc));
    }
}

void Connection::setURL(CURL* curl, const std::string& url)
{
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    if (CURLE_OK != rc)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(Connection::error("Error setting URL", rc));
    }
}

void Connection::setVerbose(CURL* curl, bool verbose)
{
    long value = (verbose) ? 1 : 0;
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_VERBOSE, value);
    if (CURLE_OK != rc)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(
                Connection::error("Error setting verbose", rc));
    }
}

void Connection::setTimeout(CURL* curl, long timeout)
{
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout);
    if (CURLE_OK != rc)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(
                Connection::error("Error setting timeout", rc));
    }
}

std::string Connection::error(CURLcode rc)
{
    std::stringstream strbuf;

    strbuf << curl_easy_strerror(rc) << " (" << rc << ')';
    return strbuf.str();
}

std::string Connection::error(const std::string& message, CURLcode rc)
{
    std::stringstream strbuf;
    strbuf << message << ": " << Connection::error(rc);
    return strbuf.str();
}

}

