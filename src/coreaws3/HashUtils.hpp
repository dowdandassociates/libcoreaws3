/*
 *
 * libcoreaws3/src/coreaws3/HashUtils.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__HASH_UTILS
#define COREAWS3__HASH_UTILS

#include <string>

#include "Buffer.hpp"

namespace coreaws3
{

class HashUtils
{
public:
    static Buffer hash(const std::string& algorithm, const Buffer& data);
    static Buffer hmacHash(const std::string& algorithm,
                           const Buffer& key,
                           const Buffer& data);
};

}

#endif // not COREAWS3__HASH_UTILS

