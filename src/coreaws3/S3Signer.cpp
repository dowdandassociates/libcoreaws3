/*
 *
 * libcoreaws3/src/coreaws3/S3Signer.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "S3Signer.hpp"

#include <algorithm>
#include <numeric>
#include <sstream>
#include <stdexcept>

#include "DateTimeUtils.hpp"
#include "HttpUtils.hpp"
#include "Maybe.hpp"
#include "StringUtils.hpp"

namespace coreaws3
{

RequestHandle S3Signer::sign(CredentialsHandle credentials,
                             RequestHandle request,
                             std::time_t time)
{
    if (!credentials)
    {
        RequestHandle unsignedRequest(new Request(
                request->httpMethod,
                request->endpoint,
                request->resourcePath,
                request->parameters,
                request->headers,
                request->content));
    }

    HeaderMap headers(Signer::filterAuthorization(request->headers));
    HeaderMap lowerHeaders(Signer::lowerCaseKey(headers));

    if (lowerHeaders.find("date") == lowerHeaders.end() &&
            lowerHeaders.find("x-amz-date") == lowerHeaders.end())
    {
        std::string key = "Date";
        Maybe<std::string>::Type value =
                Maybe<std::string>::just(DateTimeUtils::formatRFC822(time));

        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    if (credentials->sessionToken &&
            lowerHeaders.find("x-amz-security-token") == lowerHeaders.end())
    {
        std::string key = "x-amz-security-token";
        Maybe<std::string>::Type value = credentials->sessionToken;
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    RequestHandle presignedRequest(new Request(
            request->httpMethod,
            request->endpoint,
            request->resourcePath,
            request->parameters,
            headers,
            request->content));

    std::string stringToSign = S3Signer::stringToSign(presignedRequest);

    std::string signature = Signer::sign("HmacSHA1",
                                         credentials->secretAccessKey,
                                         stringToSign);

    std::stringstream authorizationValue;
    authorizationValue << "AWS " 
                       << credentials->accessKeyId
                       << ':'
                       << signature;

    headers.insert(HeaderEntry(
            "Authorization",
            Maybe<std::string>::just(authorizationValue.str())));

    RequestHandle signedRequest(new Request(
            request->httpMethod,
            request->endpoint,
            request->resourcePath,
            request->parameters,
            headers,
            request->content));

    return signedRequest;
}

S3Signer::S3Signer()
{
}

S3Signer::~S3Signer()
{
}

RequestHandle S3Signer::operator()(CredentialsHandle credentials,
                                   RequestHandle request,
                                   std::time_t time) const
{
    return S3Signer::sign(credentials, request, time);
}

std::string S3Signer::stringToSign(RequestHandle request)
{
    HeaderMap lowerHeaders(Signer::lowerCaseKey(request->headers));
    
    HeaderMap interestingHeaders;
    std::remove_copy_if(lowerHeaders.begin(),
                        lowerHeaders.end(),
                        std::inserter(interestingHeaders,
                                      interestingHeaders.end()),
                        S3Signer::isNotInterestingHeader);

    HeaderMap::const_iterator ptr = lowerHeaders.find("date");
    Maybe<std::string>::Type date = (ptr != lowerHeaders.end() &&
            lowerHeaders.find("x-amz-date") == lowerHeaders.end()) ?
            ptr->second : Maybe<std::string>::just("");
    interestingHeaders.insert(HeaderEntry("date", date));

    if (interestingHeaders.find("content-type") == interestingHeaders.end())
    {
        interestingHeaders.insert(HeaderEntry(
                "content-type",
                Maybe<std::string>::just("")));
    }

    if (interestingHeaders.find("content-md5") == interestingHeaders.end())
    {
        interestingHeaders.insert(HeaderEntry(
                "content-md5",
                Maybe<std::string>::just("")));
    }

    ParameterMap interestingParameters;
    std::remove_copy_if(request->parameters.begin(),
                        request->parameters.end(),
                        std::inserter(interestingParameters,
                                      interestingParameters.end()),
                        S3Signer::isNotInterestingParameter);

    HeaderMap headers = std::accumulate(
            interestingParameters.begin(),
            interestingParameters.end(),
            interestingHeaders,
            S3Signer::accumulateInterestingParameters);

    std::string headersToSign = std::accumulate(headers.begin(),
                                                headers.end(),
                                                std::string(),
                                                S3Signer::buildHeaderSection);

    ParameterMap parameters;
    std::remove_copy_if(request->parameters.begin(),
                        request->parameters.end(),
                        std::inserter(parameters, parameters.end()),
                        S3Signer::isNotCanonicalParameter);

    std::string queryString = S3Signer::constructQueryString(parameters);

    std::stringstream strbuf;
    strbuf << HttpMethod::show(request->httpMethod) << '\n';
    strbuf << headersToSign;

    if (StringUtils::isEmpty(request->resourcePath))
    {
        strbuf << '/';
    }
    else
    {
        strbuf << HttpUtils::urlEncodePath(request->resourcePath);
    }

    if (!StringUtils::isEmpty(queryString))
    {
        strbuf << '?' << queryString;
    }

    return strbuf.str();
}

bool S3Signer::isNotInterestingHeader(const HeaderEntry& header)
{
    if (header.first == "content-type")
    {
        return false;
    }

    if (header.first == "content-md5")
    {
        return false;
    }

    if (StringUtils::startsWith(header.first, "x-amz-"))
    {
        return false;
    }

    return true;
}

bool S3Signer::isNotInterestingParameter(const ParameterEntry& header)
{
    if (StringUtils::startsWith(header.first, "x-amz-"))
    {
        return false;
    }

    return true;
}

HeaderMap S3Signer::accumulateInterestingParameters(
        const HeaderMap& acc,
        const ParameterEntry& parameter)
{
    HeaderMap headers = acc;
    std::string value = (parameter.second) ? *(parameter.second) : "";
    headers.insert(HeaderEntry(
            parameter.first,
            Maybe<std::string>::just(value)));
    return headers;
}

std::string S3Signer::buildHeaderSection(const std::string& acc,
                                         const HeaderEntry& header)
{
    std::stringstream strbuf;

    std::string value = (header.second) ? *(header.second) : "";

    strbuf << acc;
    if (StringUtils::startsWith(header.first, "x-amz-"))
    {
        strbuf << header.first << ':' << value;
    }
    else
    {
        strbuf << value;
    }

    strbuf << '\n';

    return strbuf.str();
}

bool S3Signer::isNotCanonicalParameter(const ParameterEntry& parameter)
{
    if (parameter.first == "acl")
    {
        return false;
    }

    if (parameter.first == "torrent")
    {
        return false;
    }

    if (parameter.first == "logging")
    {
        return false;
    }

    if (parameter.first == "location")
    {
        return false;
    }

    if (parameter.first == "policy")
    {
        return false;
    }

    if (parameter.first == "requestPayment")
    {
        return false;
    }

    if (parameter.first == "versioning")
    {
        return false;
    }

    if (parameter.first == "versions")
    {
        return false;
    }

    if (parameter.first == "versionId")
    {
        return false;
    }

    if (parameter.first == "notification")
    {
        return false;
    }

    if (parameter.first == "uploadId")
    {
        return false;
    }

    if (parameter.first == "uploads")
    {
        return false;
    }

    if (parameter.first == "partNumber")
    {
        return false;
    }

    if (parameter.first == "website")
    {
        return false;
    }

    if (parameter.first == "delete")
    {
        return false;
    }

    if (parameter.first == "lifecycle")
    {
        return false;
    }

    if (parameter.first == "tagging")
    {
        return false;
    }

    if (parameter.first == "cors")
    {
        return false;
    }

    if (parameter.first == "response-cache-control")
    {
        return false;
    }

    if (parameter.first == "response-content-disposition")
    {
        return false;
    }

    if (parameter.first == "response-content-encoding")
    {
        return false;
    }

    if (parameter.first == "response-content-language")
    {
        return false;
    }

    if (parameter.first == "response-content-type")
    {
        return false;
    }

    if (parameter.first == "response-expires")
    {
        return false;
    }

    return true;
}

std::string S3Signer::constructQueryString(const ParameterMap& parameters)
{
    return S3Signer::checkQueryString(
            std::accumulate(parameters.begin(),
                            parameters.end(),
                            std::string(),
                            S3Signer::buildQueryString));
}


std::string S3Signer::checkQueryString(const std::string& queryString)
{
    if (!queryString.empty())
    {
        return queryString.substr(1);
    }
    else
    {
        return queryString;
    }
}


std::string S3Signer::buildQueryString(const std::string& acc,
                                       const ParameterEntry& parameter)
{
    std::stringstream strbuf;
    strbuf << '&';
    strbuf << parameter.first;
    if (parameter.second)
    {
        strbuf << '=' << *(parameter.second);
    }
    return acc + strbuf.str();
}


}

