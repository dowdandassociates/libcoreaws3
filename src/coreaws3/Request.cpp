/*
 *
 * libcoreaws3/src/coreaws3/Request.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "Request.hpp"

namespace coreaws3
{

Request::Request(HttpMethod::Type httpMethod,
                 EndpointHandle endpoint,
                 const std::string& resourcePath,
                 const ParameterMap& parameters,
                 const HeaderMap& headers,
                 Maybe<InputStreamHandle>::Type content) :
        httpMethod(httpMethod),
        endpoint(endpoint),
        resourcePath(resourcePath),
        parameters(parameters),
        headers(headers),
        content(content)
{
}

Request::Request(HttpMethod::Type httpMethod,
                 EndpointHandle endpoint,
                 const std::string& resourcePath,
                 const ParameterMap& parameters,
                 const HeaderMap& headers,
                 InputStreamHandle content) :
        httpMethod(httpMethod),
        endpoint(endpoint),
        resourcePath(resourcePath),
        parameters(parameters),
        headers(headers),
        content(Maybe<InputStreamHandle>::just(content))
{
}

Request::Request(HttpMethod::Type httpMethod,
                 EndpointHandle endpoint,
                 const std::string& resourcePath,
                 const ParameterMap& parameters,
                 const HeaderMap& headers) :
        httpMethod(httpMethod),
        endpoint(endpoint),
        resourcePath(resourcePath),
        parameters(parameters),
        headers(headers),
        content(Maybe<InputStreamHandle>::nothing())
{
}

Request::~Request()
{
}

}

