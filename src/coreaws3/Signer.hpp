/*
 *
 * libcoreaws3/src/coreaws3/Signer.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__SIGNER
#define COREAWS3__SIGNER

#include <ctime>
#include <string>

#include "Buffer.hpp"
#include "CredentialsHandle.hpp"
#include "CredentialsProviderHandle.hpp"
#include "EndpointHandle.hpp"
#include "HeaderEntry.hpp"
#include "HeaderMap.hpp"
#include "ParameterEntry.hpp"
#include "ParameterMap.hpp"
#include "RequestHandle.hpp"

namespace coreaws3
{

class Signer
{
public:
    Signer();
    virtual ~Signer();
    virtual RequestHandle operator()(CredentialsHandle credentials,
                                     RequestHandle request,
                                     std::time_t time) const = 0;
    virtual RequestHandle operator()(CredentialsProviderHandle credentials,
                                     RequestHandle request,
                                     std::time_t time) const;

protected:
    static std::string sign(const std::string& algorithm,
                            const Buffer& key,
                            const Buffer& data);
    static std::string sign(const std::string& algorithm,
                            const Buffer& key,
                            const std::string data);
    static std::string sign(const std::string& algorithm,
                            const std::string& key,
                            const Buffer& data);
    static std::string sign(const std::string& algorithm,
                            const std::string& key,
                            const std::string& data);

    static std::string canonicalizedEndpoint(EndpointHandle endpoint);
    static std::string canonicalizedQueryString(const ParameterMap& parameters);
    static std::string canonicalizedResourcePath(
            const std::string& resourcePath);

    static ParameterMap filterSignature(const ParameterMap& parameters);
    static HeaderMap lowerCaseKey(const HeaderMap& headers);
    static HeaderMap filterAuthorization(const HeaderMap& headers);
    static HeaderMap filterXAmznAuthorization(const HeaderMap& headers);

private:
    static std::string checkQueryString(const std::string& queryString);
    static std::string buildQueryString(const std::string& acc,
                                        const ParameterEntry& parameter);

    static bool isSignature(const ParameterEntry& parameter);
    static bool isAuthorization(const HeaderEntry& header);
    static bool isXAmznAuthorization(const HeaderEntry& header);
    static HeaderEntry keyToLowerCase(const HeaderEntry& header);
};

}

#endif // not COREAWS3__SIGNER

