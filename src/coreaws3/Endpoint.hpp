/*
 *
 * libcoreaws3/src/coreaws3/Endpoint.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__ENDPOINT
#define COREAWS3__ENDPOINT

#include <string>

#include "Maybe.hpp"
#include "Port.hpp"
#include "Scheme.hpp"

namespace coreaws3
{

class Endpoint
{
public:
    Endpoint(Scheme::Type scheme,
             const std::string& host,
             Maybe<Port>::Type port);
    Endpoint(Scheme::Type scheme,
             const std::string& host);
    Endpoint(Scheme::Type scheme,
             const std::string& host,
             Port port);

    virtual ~Endpoint();
    virtual std::string toString() const;
    virtual bool isUsingDefaultPort() const;
    virtual std::string server() const;

    const Scheme::Type scheme;
    const std::string host;
    const Maybe<Port>::Type port;

private:
    Endpoint(const Endpoint&); // not implemented
    void operator=(const Endpoint&); // not implemented
};

}

#endif // not COREAWS__ENDPOINT

