/*
 *
 * libcoreaws3/src/coreaws3/HttpRequest.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "HttpRequest.hpp"

namespace coreaws3
{
HttpRequest::HttpRequest(HttpMethod::Type method,
                         const std::string& url,
                         const std::map<std::string, std::string>& headers,
                         InputStreamHandle content) :
        method(method),
        url(url),
        headers(headers),
        content(content),
        timeout(HttpRequest::TIMEOUT),
        verbose(HttpRequest::VERBOSE)
{
}

HttpRequest::HttpRequest(HttpMethod::Type method,
                         const std::string& url,
                         const std::map<std::string, std::string>& headers,
                         InputStreamHandle content,
                         long timeout,
                         bool verbose) :
        method(method),
        url(url),
        headers(headers),
        content(content),
        timeout(timeout),
        verbose(verbose)
{
}

HttpRequest::~HttpRequest()
{
}

}

