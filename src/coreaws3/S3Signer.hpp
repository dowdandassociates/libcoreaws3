/*
 *
 * libcoreaws3/src/coreaws3/S3Signer.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__S3_SIGNER
#define COREAWS3__S3_SIGNER

#include "Signer.hpp"

namespace coreaws3
{

class S3Signer : public Signer
{
public:
    static RequestHandle sign(CredentialsHandle credentials,
                              RequestHandle request,
                              std::time_t time);

    S3Signer();
    virtual ~S3Signer();
    virtual RequestHandle operator()(CredentialsHandle credentials,
                                     RequestHandle request,
                                     std::time_t time) const;

protected:
    static std::string stringToSign(RequestHandle request);

private:
    static bool isNotInterestingHeader(const HeaderEntry& header);
    static bool isNotInterestingParameter(const ParameterEntry& header);
    static HeaderMap accumulateInterestingParameters(
            const HeaderMap& acc,
            const ParameterEntry& parameter);
    static std::string buildHeaderSection(const std::string& acc,
                                          const HeaderEntry& header);
    static bool isNotCanonicalParameter(const ParameterEntry& parameter);
    static std::string constructQueryString(const ParameterMap& parameters);
    static std::string checkQueryString(const std::string& queryString);
    static std::string buildQueryString(const std::string& acc,
                                        const ParameterEntry& parameter);
};

}

#endif // not COREAWS3__S3_SIGNER

