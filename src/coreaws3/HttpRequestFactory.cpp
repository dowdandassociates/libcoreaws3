/*
 *
 * libcoreaws3/src/coreaws3/HttpRequestFactory.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "HttpRequestFactory.hpp"

#include <algorithm>
#include <sstream>

#include "HttpUtils.hpp"
#include "StreamUtils.hpp"
#include "StringUtils.hpp"

namespace coreaws3
{

HttpRequestHandle HttpRequestFactory::convertRequest(RequestHandle request)
{
    return HttpRequestFactory::convertRequest(
            request, HttpRequest::TIMEOUT, HttpRequest::VERBOSE);
}

HttpRequestHandle HttpRequestFactory::convertRequest(
        RequestHandle request, long timeout, bool verbose)
{
    std::stringstream url;
    url << Scheme::show(request->endpoint->scheme);
    url << "://";
    url << request->endpoint->host;
    if (!request->endpoint->isUsingDefaultPort())
    {
        url << ':' << *(request->endpoint->port);
    }

    if (StringUtils::isEmpty(request->resourcePath))
    {
        url << "/";
    }
    else
    {
        url << HttpUtils::urlEncodePath(request->resourcePath);
    }

    std::size_t contentSize = HttpRequestFactory::getContentSize(request);
    std::string queryString = HttpUtils::encodeParameters(request);
    InputStreamHandle content;

    if (request->httpMethod != HttpMethod::POST || contentSize != 0)
    {
        if (queryString.size() != 0)
        {
            url << '?' << queryString;
        }

        if (request->content)
        {
            content = *(request->content);
        }
        else
        {
            content = StreamUtils::emptyInputStream();
        }
    }
    else
    {
        content = StreamUtils::setInput(queryString);
    }

    HttpRequestHandle httpRequest(new HttpRequest(
            request->httpMethod,
            url.str(),
            HttpRequestFactory::processHeaders(request->headers),
            content,
            timeout,
            verbose));

    return httpRequest;
}

std::size_t HttpRequestFactory::getContentSize(RequestHandle request)
{
    if (!request->content)
    {
        return 0;
    }
    else
    {
        return StreamUtils::getInputSize(*(request->content));
    }
}

std::map<std::string, std::string> HttpRequestFactory::processHeaders(
        const HeaderMap& headers)
{
    std::map<std::string, std::string> httpHeaders;
    std::transform(headers.begin(),
                   headers.end(),
                   std::inserter(httpHeaders, httpHeaders.end()),
                   HttpRequestFactory::processHeader);
    return httpHeaders;
}

std::pair<const std::string, std::string> HttpRequestFactory::processHeader(
        const HeaderEntry& header)
{
    std::string key = header.first;
    std::string value = (header.second) ? *(header.second) : "";
    return std::pair<const std::string, std::string>(key, value);
}

}

