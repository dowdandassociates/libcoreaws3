/*
 *
 * libcoreaws3/src/coreaws3/BinaryUtils.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "BinaryUtils.hpp"

#include <algorithm>
#include <cstring>
#include <numeric>
#include <stdexcept>

#include <openssl/bio.h>
#include <openssl/buffer.h>
#include <openssl/evp.h>

#include "StringUtils.hpp"

namespace coreaws3
{

Buffer BinaryUtils::fromArray(const unsigned char* data, std::size_t size)
{
    Buffer buffer;
    for (std::size_t i = 0; i < size; ++i)
    {
        buffer.push_back(data[i]);
    }

    return buffer; 
}

Buffer BinaryUtils::fromBase64(const std::string& data)
{
    int len = data.size();
    unsigned char* buf = new unsigned char[len];
    memset(buf, 0, len);
    BIO* b64 = BIO_new(BIO_f_base64());
    BIO* bmem = BIO_new_mem_buf((void*)data.data(), len);
    bmem = BIO_push(b64, bmem);
    BIO_read(bmem, buf, len);
    Buffer buffer = BinaryUtils::fromArray(buf, len);
    BIO_free_all(bmem);
    delete [] buf;

    return buffer;
}

Buffer BinaryUtils::fromHex(const std::string& data)
{
    static const unsigned char* const lut =
            (const unsigned char*)"0123456789ABCDEF";
    std::size_t len = data.length();
    if (len & 1)
    {
        throw std::invalid_argument("odd length");
    }

    std::string input = StringUtils::toUpperCase(data);
    Buffer output;
    for (std::size_t i = 0; i < len; i += 2)
    {
        unsigned char a = input[i];
        const unsigned char* p = std::lower_bound(lut, lut + 16, a);
        if (*p != a)
        {
            throw std::invalid_argument("not a hex digit");
        }

        unsigned char b = input[i + 1];
        const unsigned char* q = std::lower_bound(lut, lut + 16, b);
        if (*q != b)
        {
            throw std::invalid_argument("not a hex digit");
        }

        output.push_back(((p - lut) << 4 | (q - lut)));
    }

    return output;
}

Buffer BinaryUtils::fromString(const std::string& str)
{
    return Buffer(str.begin(), str.end());
}

std::string BinaryUtils::toBase64(const Buffer& data)
{
    BIO* b64 = BIO_new(BIO_f_base64());
    BIO* bio = BIO_new(BIO_s_mem());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    bio = BIO_push(b64, bio);
    BIO_write(bio, data.data(), data.size());
    BIO_flush(bio);

    char* tmp;
    long len = BIO_get_mem_data(bio, &tmp);
    char* encoded = new char[len + 1];
    memset(encoded, 0, len + 1);
    memcpy(encoded, tmp, len);

    std::string base64 = encoded;

    delete [] encoded;
    BIO_free_all(bio);

    return base64;
}

std::string BinaryUtils::toHex(const Buffer& data)
{
    return std::accumulate(data.begin(),
                           data.end(),
                           std::string(),
                           BinaryUtils::buildHex);
}

std::string BinaryUtils::toString(const Buffer& data)
{
    return std::string(data.begin(), data.end());
}

std::string BinaryUtils::buildHex(const std::string& acc, unsigned char c)
{
    char buf[3];
    memset(buf, 0, 3);

    sprintf(buf, "%02x", c);

    return acc + std::string(buf);
}

}

