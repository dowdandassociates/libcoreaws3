/*
 *
 * libcoreaws3/src/coreaws3/StringUtils.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__STRING_UTILS
#define COREAWS3__STRING_UTILS

#include <string>

namespace coreaws3
{

class StringUtils
{
public:
    static bool endsWith(const std::string& haystack,
                         const std::string& needle);
    static bool endsWithIgnoreCase(const std::string& haystack,
                                   const std::string& needle);
    static bool equalsIgnoreCase(const std::string& lhs,
                                 const std::string& rhs);
    static bool isEmpty(const std::string& str);
    static bool isBlank(const std::string& str);
    static std::string ltrim(const std::string& str);
    static std::string rtrim(const std::string& str);
    static bool startsWith(const std::string& haystack,
                           const std::string& needle);
    static bool startsWithIgnoreCase(const std::string& haystack,
                                     const std::string& needle);
    static std::string toLowerCase(const std::string& str);
    static std::string toUpperCase(const std::string& str);
    static std::string trim(const std::string& str);
};

}

#endif // not COREAWS3__STRING_UTILS

