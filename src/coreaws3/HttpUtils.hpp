/*
 *
 * libcoreaws3/src/coreaws3/HttpUtils.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__HTTP_UTILS
#define COREAWS3__HTTP_UTILS

#include <string>

#include "ParameterEntry.hpp"
#include "RequestHandle.hpp"

namespace coreaws3
{

class HttpUtils
{
public:
    static std::string encodeParameters(RequestHandle request);
    static std::string urlEncode(const std::string& str);
    static std::string urlEncodePath(const std::string& path);
private:
    static std::string urlEncodeCharacter(const std::string& acc, char c);
    static std::string urlEncodePathCharacter(const std::string& acc, char c);
    static std::string escapeCharacter(char c);
    static bool isNormalCharacter(char c);
    static bool isNormalPathCharacter(char c);
    static std::string checkQueryString(const std::string& queryString);
    static std::string buildQueryString(const std::string& acc,
                                        const ParameterEntry& parameter);
};

}

#endif // not COREAWS3__HTTP_UTILS

