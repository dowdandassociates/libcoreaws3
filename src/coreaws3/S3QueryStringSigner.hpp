/*
 *
 * libcoreaws3/src/coreaws3/S3QueryStringSigner.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__S3_QUERY_STRING_SIGNER
#define COREAWS3__S3_QUERY_STRING_SIGNER

#include "S3Signer.hpp"

namespace coreaws3
{

class S3QueryStringSigner : public S3Signer
{
public:
    static RequestHandle sign(CredentialsHandle credentials,
                              RequestHandle request,
                              std::time_t expires);

    S3QueryStringSigner();
    virtual ~S3QueryStringSigner();
    virtual RequestHandle operator()(CredentialsHandle credentials,
                                     RequestHandle request,
                                     std::time_t expires) const;
};

}

#endif // not COREAWS3__S3_QUERY_STRING_SIGNER

