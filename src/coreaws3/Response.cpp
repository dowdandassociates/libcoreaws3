/*
 *
 * libcoreaws3/src/coreaws3/Response.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "Response.hpp"

#include <sstream>
#include <stdexcept>

namespace coreaws3
{
Response::Response(CURL* curl,
                   CURLcode returnCode,
                   char* errorMessage,
                   curl_slist* headerList,
                   OutputStreamHandle output,
                   OutputStreamHandle header) :
        curl(curl),
        returnCode(returnCode),
        errorMessage(errorMessage),
        headerList(headerList),
        output(output),
        header(header),
        errorBuffer(errorMessage)
{
}

Response::~Response()
{
    curl_easy_cleanup(this->curl);
    if (NULL != errorBuffer)
    {
        delete [] errorBuffer;
    }
    if (NULL != headerList)
    {
        curl_slist_free_all(headerList);
    }
}

long Response::getResponseCode() const
{
    long responseCode;
    CURLcode rc = this->getInfo(CURLINFO_RESPONSE_CODE, &responseCode);
    if (rc != CURLE_OK)
    {
        std::stringstream errbuf;
        errbuf << "Error getting response code: "
               << curl_easy_strerror(rc) << " (" << rc << ')';
        throw std::runtime_error(errbuf.str());
    }

    return responseCode;
}

CURLcode Response::getInfo(CURLINFO info, long* data) const
{
    return curl_easy_getinfo(this->curl, info, data);
}

CURLcode Response::getInfo(CURLINFO info, char** data) const
{
    return curl_easy_getinfo(this->curl, info, data);
}

CURLcode Response::getInfo(CURLINFO info, curl_slist** data) const
{
    return curl_easy_getinfo(this->curl, info, data);
}

CURLcode Response::getInfo(CURLINFO info, double* data) const
{
    return curl_easy_getinfo(this->curl, info, data);
}

}

