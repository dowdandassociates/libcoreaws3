/*
 *
 * libcoreaws3/src/coreaws3/SignerHandle.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__SIGNER_HANDLE
#define COREAWS3__SIGNER_HANDLE

#include <tr1/memory>

#include "Signer.hpp"

namespace coreaws3
{

typedef std::tr1::shared_ptr<Signer> SignerHandle;

}

#endif // not COREAWS3__SIGNER_HANDLE

