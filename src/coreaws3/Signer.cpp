/*
 *
 * libcoreaws3/src/coreaws3/Signer.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "Signer.hpp"

#include <algorithm>
#include <iterator>
#include <numeric>
#include <sstream>
#include <stdexcept>

#include "BinaryUtils.hpp"
#include "HashUtils.hpp"
#include "HttpUtils.hpp"
#include "StringUtils.hpp"

namespace coreaws3
{

Signer::Signer()
{
}

Signer::~Signer()
{
}

RequestHandle Signer::operator()(CredentialsProviderHandle credentials,
                                 RequestHandle request,
                                 std::time_t time) const
{
    return (*this)((*credentials)(), request, time);
}

std::string Signer::sign(const std::string& algorithm,
                         const Buffer& key,
                         const Buffer& data)
{
    std::string digest;
    if (algorithm == "HmacSHA1")
    {
        digest = "sha1";
    }
    else if (algorithm == "HmacSHA256")
    {
        digest = "sha256";
    }
    else
    {
        std::stringstream errbuf;
        errbuf << "Unknown algorithm: " << algorithm;
        throw std::invalid_argument(errbuf.str());
    }

    return BinaryUtils::toBase64(HashUtils::hmacHash(digest, key, data));
}

std::string Signer::sign(const std::string& algorithm,
                         const Buffer& key,
                         const std::string data)
{
    return Signer::sign(algorithm, key, BinaryUtils::fromString(data));
}

std::string Signer::sign(const std::string& algorithm,
                         const std::string& key,
                         const Buffer& data)
{
    return Signer::sign(algorithm, BinaryUtils::fromString(key), data);
}

std::string Signer::sign(const std::string& algorithm,
                         const std::string& key,
                         const std::string& data)
{
    return Signer::sign(algorithm,
                        BinaryUtils::fromString(key),
                        BinaryUtils::fromString(data));
}

std::string Signer::canonicalizedEndpoint(EndpointHandle endpoint)
{
    return StringUtils::toLowerCase(endpoint->server());
}

std::string Signer::canonicalizedQueryString(const ParameterMap& parameters)
{
    return Signer::checkQueryString(
            std::accumulate(parameters.begin(),
                            parameters.end(),
                            std::string(),
                            Signer::buildQueryString));
}

std::string Signer::canonicalizedResourcePath(const std::string& resourcePath)
{
    if (StringUtils::isEmpty(resourcePath))
    {
        return "/";
    }
    else
    {
        return HttpUtils::urlEncodePath(resourcePath);
    }
}

std::string Signer::checkQueryString(const std::string& queryString)
{
    if (!queryString.empty())
    {
        return queryString.substr(1);
    }
    else
    {
        return queryString;
    }
}

std::string Signer::buildQueryString(const std::string& acc,
                                     const ParameterEntry& parameter)
{
    std::stringstream strbuf;
    strbuf << '&';
    strbuf << HttpUtils::urlEncode(parameter.first);
    strbuf << '=';
    if (parameter.second)
    {
        strbuf << HttpUtils::urlEncode(*(parameter.second));
    }
    return acc + strbuf.str();
}

ParameterMap Signer::filterSignature(const ParameterMap& parameters)
{
    ParameterMap newParameters;
    std::remove_copy_if(parameters.begin(),
                        parameters.end(),
                        std::inserter(newParameters, newParameters.end()),
                        Signer::isSignature);

    return newParameters;
}

bool Signer::isSignature(const ParameterEntry& parameter)
{
    return (parameter.first == "Signature");
}

HeaderMap Signer::lowerCaseKey(const HeaderMap& headers)
{
    HeaderMap lowerHeaders;
    std::transform(headers.begin(),
                   headers.end(),
                   std::inserter(lowerHeaders, lowerHeaders.end()),
                   Signer::keyToLowerCase);
    return lowerHeaders;
}

HeaderMap Signer::filterAuthorization(const HeaderMap& headers)
{
    HeaderMap newHeaders;
    std::remove_copy_if(headers.begin(),
                        headers.end(),
                        std::inserter(newHeaders, newHeaders.end()),
                        Signer::isAuthorization);
    return newHeaders;
}

HeaderMap Signer::filterXAmznAuthorization(const HeaderMap& headers)
{
    HeaderMap newHeaders;
    std::remove_copy_if(headers.begin(),
                        headers.end(),
                        std::inserter(newHeaders, newHeaders.end()),
                        Signer::isXAmznAuthorization);
    return newHeaders;
}

bool Signer::isAuthorization(const HeaderEntry& header)
{
    return (StringUtils::toLowerCase(header.first) == "authorization");
}

bool Signer::isXAmznAuthorization(const HeaderEntry& header)
{
    return (StringUtils::toLowerCase(header.first) == "x-amzn-authorization");
}

HeaderEntry Signer::keyToLowerCase(const HeaderEntry& header)
{
    return HeaderEntry(StringUtils::toLowerCase(header.first), header.second);
}

}

