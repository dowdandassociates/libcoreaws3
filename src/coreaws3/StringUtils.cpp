/*
 *
 * libcoreaws3/src/coreaws3/StringUtils.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "StringUtils.hpp"

#include <algorithm>
#include <cctype>
#include <functional>
#include <locale>

namespace coreaws3
{
bool StringUtils::endsWith(const std::string& haystack,
                           const std::string& needle)
{
    if (haystack.length() >= needle.length())
    {
        return (haystack.compare(haystack.length() - needle.length(),
                                 needle.length(),
                                 needle) == 0);
    }
    else
    {
        return false;
    }
}

bool StringUtils::endsWithIgnoreCase(const std::string& haystack,
                                     const std::string& needle)
{
    return (StringUtils::endsWith(StringUtils::toLowerCase(haystack),
                                  StringUtils::toLowerCase(needle)));
}

bool StringUtils::equalsIgnoreCase(const std::string& lhs,
                                   const std::string& rhs)
{
    return (StringUtils::toLowerCase(lhs) == StringUtils::toLowerCase(rhs));
}

bool StringUtils::isEmpty(const std::string& str)
{
    return (str == "");
}

bool StringUtils::isBlank(const std::string& str)
{
    return isEmpty(StringUtils::trim(str));
}

std::string StringUtils::ltrim(const std::string& str)
{
    std::string temp = str;
    temp.erase(temp.begin(),
               std::find_if(temp.begin(),
                            temp.end(),
                            std::not1(std::ptr_fun<int, int>(std::isspace))));
    return temp;
}

std::string StringUtils::rtrim(const std::string& str)
{
    std::string temp = str;
    temp.erase(std::find_if(temp.rbegin(),
                            temp.rend(),
                            std::not1(std::ptr_fun<int, int>(std::isspace))).
                       base(),
               temp.end());
    return temp;
}

bool StringUtils::startsWith(const std::string& haystack,
                             const std::string& needle)
{
    return (haystack.find(needle) == 0);
}

bool StringUtils::startsWithIgnoreCase(const std::string& haystack,
                                       const std::string& needle)
{
    return (StringUtils::startsWith(StringUtils::toLowerCase(haystack),
                                    StringUtils::toLowerCase(needle)));
}

std::string StringUtils::toLowerCase(const std::string& str)
{
    std::string lowercase(str.length(), '\0');
    std::transform(str.begin(), str.end(), lowercase.begin(), ::tolower);
    return lowercase;
}

std::string StringUtils::toUpperCase(const std::string& str)
{
    std::string uppercase(str.length(), '\0');
    std::transform(str.begin(), str.end(), uppercase.begin(), ::toupper);
    return uppercase;
}

std::string StringUtils::trim(const std::string& str)
{
    return ltrim(rtrim(str));
}

}

