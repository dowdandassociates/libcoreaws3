/*
 *
 * libcoreaws3/src/coreaws3/HttpMethod.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "HttpMethod.hpp"

#include <sstream>
#include <stdexcept>

namespace coreaws3
{

HttpMethod::Type HttpMethod::read(const std::string& httpMethod)
{
    if (httpMethod == "GET")
    {
        return HttpMethod::GET;
    }
    else if (httpMethod == "POST")
    {
        return HttpMethod::POST;
    }
    else if (httpMethod == "PUT")
    {
        return HttpMethod::PUT;
    }
    else if (httpMethod == "DELETE")
    {
        return HttpMethod::DELETE;
    }
    else if (httpMethod == "HEAD")
    {
        return HttpMethod::HEAD;
    }
    else
    {
        std::stringstream errbuf;
        errbuf << "Unknown HttpMethod: " << httpMethod;
        throw std::invalid_argument(errbuf.str());
    }
}

std::string HttpMethod::show(HttpMethod::Type httpMethod)
{
    switch (httpMethod)
    {
    case HttpMethod::GET:
        return "GET";
    case HttpMethod::POST:
        return "POST";
    case HttpMethod::PUT:
        return "PUT";
    case HttpMethod::DELETE:
        return "DELETE";
    case HttpMethod::HEAD:
        return "HEAD";
    default:
        throw std::invalid_argument("Unknown HttpMethod");
    }
}

}

