/*
 *
 * libcoreaws3/src/coreaws3/CloudFrontSigner.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "CloudFrontSigner.hpp"

#include <sstream>

#include "DateTimeUtils.hpp"
#include "StringUtils.hpp"

namespace coreaws3
{

RequestHandle CloudFrontSigner::sign(CredentialsHandle credentials,
                                     RequestHandle request,
                                      std::time_t time)
{
    HeaderMap headers(Signer::filterAuthorization(request->headers));
    HeaderMap lowerHeaders(Signer::lowerCaseKey(headers));

    if (lowerHeaders.find("date") == lowerHeaders.end() &&
            lowerHeaders.find("x-amz-date") == lowerHeaders.end())
    {
        std::string key = "Date";
        Maybe<std::string>::Type value =
                Maybe<std::string>::just(DateTimeUtils::formatRFC822(time));
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    if (credentials->sessionToken &&
            lowerHeaders.find("x-amz-security-token") == lowerHeaders.end())
    {
        std::string key = "x-amz-security-token";
        Maybe<std::string>::Type value = credentials->sessionToken;
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    RequestHandle presignedRequest(new Request(
            request->httpMethod,
            request->endpoint,
            request->resourcePath,
            request->parameters,
            headers,
            request->content));

    std::string stringToSign = CloudFrontSigner::stringToSign(presignedRequest);

    std::string signature = Signer::sign("HmacSHA1",
                                         credentials->secretAccessKey,
                                         stringToSign);

    std::stringstream authorizationValue;
    authorizationValue << "AWS "
                       << credentials->accessKeyId
                       << ':'
                       << signature;

    headers.insert(HeaderEntry(
            "Authorization",
            Maybe<std::string>::just(authorizationValue.str())));

    RequestHandle signedRequest(new Request(
            request->httpMethod,
            request->endpoint,
            request->resourcePath,
            request->parameters,
            headers,
            request->content));

    return signedRequest;
}

CloudFrontSigner::CloudFrontSigner()
{
}

CloudFrontSigner::~CloudFrontSigner()
{
}

RequestHandle CloudFrontSigner::operator()(CredentialsHandle credentials,
                                           RequestHandle request,
                                           std::time_t time) const
{
    return CloudFrontSigner::sign(credentials, request, time);
}

std::string CloudFrontSigner::stringToSign(RequestHandle request)
{
    HeaderMap lowerHeaders(Signer::lowerCaseKey(request->headers));

    HeaderMap::const_iterator ptr;

    ptr = lowerHeaders.find("x-amz-date");
    if (ptr != lowerHeaders.end())
    {
        Maybe<std::string>::Type maybeString = ptr->second;
        if (maybeString)
        {
            return *maybeString;
        }
    }

    ptr = lowerHeaders.find("date");
    if (ptr != lowerHeaders.end())
    {
        Maybe<std::string>::Type maybeString = ptr->second;
        if (maybeString)
        {
            return *maybeString;
        }
    }

    return std::string();
}

}

