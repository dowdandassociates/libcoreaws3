/*
 *
 * libcoreaws3/src/coreaws3/AWS4Signer.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS3__AWS_4_SIGNER
#define COREAWS3__AWS_4_SIGNER

#include "Buffer.hpp"
#include "HeaderEntry.hpp"
#include "HeaderMap.hpp"
#include "Maybe.hpp"
#include "Signer.hpp"

namespace coreaws3
{

class AWS4Signer : public Signer
{
public:
    static RequestHandle sign(CredentialsHandle credentials,
                              RequestHandle request,
                              std::time_t time);

    AWS4Signer();
    virtual ~AWS4Signer();
    virtual RequestHandle operator()(CredentialsHandle credentials,
                                     RequestHandle request,
                                     std::time_t time) const;

private:
    static std::string canonicalRequest(RequestHandle request);
    static std::string canonicalHeaders(const HeaderMap& lowerHeaders);
    static std::string buildCanonicalHeaders(const std::string& acc,
                                             const HeaderEntry& header);
    static std::string signedHeaders(const HeaderMap& lowerHeaders);
    static std::string buildSignedHeaders(const std::string& acc,
                                          const HeaderEntry& header);
    static std::string payloadHash(InputStreamHandle payload);
    static std::string scope(const std::string& dateStamp,
                             const std::string& region,
                             const std::string& service,
                             const std::string& terminator);
    static std::string signingCredentials(CredentialsHandle credentials,
                                          const std::string& scope);
    static std::string stringToSign(const std::string& algorithm,
                                    const std::string& iso8601,
                                    const std::string& scope,
                                    const std::string& canonicalRequest);
    static Buffer signature(CredentialsHandle credentials,
                            const std::string& dateStamp,
                            const std::string& region,
                            const std::string& service,
                            const std::string& terminator,
                            const std::string& stringToSign);
    static Buffer kSigning(CredentialsHandle credentials,
                           const std::string& dateStamp,
                           const std::string& region,
                           const std::string& service,
                           const std::string& terminator);
    static Buffer kService(CredentialsHandle credentials,
                           const std::string& dateStamp,
                           const std::string& region,
                           const std::string& service);
    static Buffer kRegion(CredentialsHandle credentials,
                          const std::string& dateStamp,
                          const std::string& region);
    static Buffer kDate(CredentialsHandle credentials,
                        const std::string& dateStamp);
    static Buffer kSecret(CredentialsHandle credentials);
    static std::string authorizationHeaderValue(
            const std::string& algorithm,
            const std::string& signingCredentials,
            const std::string& signedHeaders,
            const Buffer& signature);
};

}

#endif // not COREAWS3__AWS_4_SIGNER

