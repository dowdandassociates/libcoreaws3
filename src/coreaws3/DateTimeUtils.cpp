/*
 *
 * libcoreaws3/src/coreaws3/DateTimeUtils.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "DateTimeUtils.hpp"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif // HAVE_CONFIG_H

#include <cstring>
#include <sstream>
#include <stdexcept>

namespace coreaws3
{

std::string DateTimeUtils::formatCompressedISO8601(std::time_t time)
{
    std::tm utc = DateTimeUtils::getUTC(time);

    std::string yyyy(DateTimeUtils::yyyy(utc));
    std::string MM(DateTimeUtils::MM(utc));
    std::string dd(DateTimeUtils::dd(utc));
    std::string HH(DateTimeUtils::HH(utc));
    std::string mm(DateTimeUtils::mm(utc));
    std::string ss(DateTimeUtils::ss(utc));

    std::stringstream strbuf;
    strbuf << yyyy << MM << dd << 'T' << HH << mm << ss << 'Z';
    return strbuf.str();
}

std::string DateTimeUtils::formatDateStamp(std::time_t time)
{
    std::tm utc = DateTimeUtils::getUTC(time);

    std::string yyyy(DateTimeUtils::yyyy(utc));
    std::string MM(DateTimeUtils::MM(utc));
    std::string dd(DateTimeUtils::dd(utc));

    std::stringstream strbuf;
    strbuf << yyyy << MM << dd;
    return strbuf.str();
}

std::string DateTimeUtils::formatISO8601(std::time_t time)
{
    std::tm utc = DateTimeUtils::getUTC(time);

    std::string yyyy(DateTimeUtils::yyyy(utc));
    std::string MM(DateTimeUtils::MM(utc));
    std::string dd(DateTimeUtils::dd(utc));
    std::string HH(DateTimeUtils::HH(utc));
    std::string mm(DateTimeUtils::mm(utc));
    std::string ss(DateTimeUtils::ss(utc));

    std::stringstream strbuf;
    strbuf << yyyy << '-' << MM << '-' << dd << 'T'
           << HH << ':' << mm << ':' << ss << 'Z';
    return strbuf.str();
}

std::string DateTimeUtils::formatRFC822(std::time_t time)
{
    std::tm utc = DateTimeUtils::getUTC(time);

    std::string EEE(DateTimeUtils::EEE(utc));
    std::string dd(DateTimeUtils::dd(utc));
    std::string MMM(DateTimeUtils::MMM(utc));
    std::string yyyy(DateTimeUtils::yyyy(utc));
    std::string HH(DateTimeUtils::HH(utc));
    std::string mm(DateTimeUtils::mm(utc));
    std::string ss(DateTimeUtils::ss(utc));

    std::stringstream strbuf;
    strbuf << EEE << ", " << dd << ' ' << MMM << ' ' << yyyy << ' '
           << HH << ':' << mm << ':' << ss << " GMT";

    return strbuf.str();
}

std::time_t DateTimeUtils::now()
{
    return std::time(NULL);
}

std::tm DateTimeUtils::getUTC(std::time_t time)
{
    struct tm utc;

#ifdef HAVE_GMTIME_R
    ::gmtime_r(&time, &utc);
#else
    std::memcpy(&utc, std::gmtime(&time), sizeof (std::tm));
#endif // HAVE_GMTIME_R

    return utc;
}

std::string DateTimeUtils::zeroPadded(int desiredLength, int num)
{
    std::stringstream valuebuf;
    valuebuf << num;
    std::string value = valuebuf.str();

    int length = desiredLength - value.size();
    if (length <= 0)
    {
        return value;
    }
    std::string formatted(length, '0');
    return formatted + value;
}

std::string DateTimeUtils::yyyy(const std::tm& utc)
{
    return DateTimeUtils::zeroPadded(4, utc.tm_year + 1900);
}

std::string DateTimeUtils::MM(const std::tm& utc)
{
    return DateTimeUtils::zeroPadded(2, utc.tm_mon + 1);
}

std::string DateTimeUtils::dd(const std::tm& utc)
{
    return DateTimeUtils::zeroPadded(2, utc.tm_mday);
}

std::string DateTimeUtils::HH(const std::tm& utc)
{
    return DateTimeUtils::zeroPadded(2, utc.tm_hour);
}

std::string DateTimeUtils::mm(const std::tm& utc)
{
    return DateTimeUtils::zeroPadded(2, utc.tm_min);
}

std::string DateTimeUtils::ss(const std::tm& utc)
{
    return DateTimeUtils::zeroPadded(2, utc.tm_sec);
}

std::string DateTimeUtils::EEE(const std::tm& utc)
{
    switch (utc.tm_wday)
    {
    case 0:
        return "Sun";
    case 1:
        return "Mon";
    case 2:
        return "Tue";
    case 3:
        return "Wed";
    case 4:
        return "Thu";
    case 5:
        return "Fri";
    case 6:
        return "Sat";
    default:
        throw std::invalid_argument("Invalid day of week");
    }
}

std::string DateTimeUtils::MMM(const std::tm& utc)
{
    switch (utc.tm_mon)
    {
    case 0:
        return "Jan";
    case 1:
        return "Feb";
    case 2:
        return "Mar";
    case 3:
        return "Apr";
    case 4:
        return "May";
    case 5:
        return "Jun";
    case 6:
        return "Jul";
    case 7:
        return "Aug";
    case 8:
        return "Sep";
    case 9:
        return "Oct";
    case 10:
        return "Nov";
    case 11:
        return "Dec";
    default:
        throw std::invalid_argument("Invalid month");
    }
}

}

