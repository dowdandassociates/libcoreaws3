/*
 *
 * libcoreaws3/src/coreaws3/AWS3HttpsSigner.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "AWS3HttpsSigner.hpp"

#include <sstream>
#include <stdexcept>

#include "DateTimeUtils.hpp"
#include "Maybe.hpp"
#include "StringUtils.hpp"

namespace coreaws3
{

RequestHandle AWS3HttpsSigner::sign(CredentialsHandle credentials,
                                   RequestHandle request,
                                   std::time_t time)
{
    std::string algorithm = "HmacSHA256";

    HeaderMap headers(Signer::filterXAmznAuthorization(request->headers));
    HeaderMap lowerHeaders(Signer::lowerCaseKey(headers));

    if (lowerHeaders.find("date") == lowerHeaders.end() &&
            lowerHeaders.find("x-amz-date") == lowerHeaders.end())
    {
        std::string key = "Date";
        Maybe<std::string>::Type value =
                Maybe<std::string>::just(DateTimeUtils::formatRFC822(time));
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    if (credentials->sessionToken &&
            lowerHeaders.find("x-amz-security-token") == lowerHeaders.end())
    {
        std::string key = "x-amz-security-token";
        Maybe<std::string>::Type value = credentials->sessionToken;
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    RequestHandle unsignedRequest(new Request(
            request->httpMethod,
            request->endpoint,
            request->resourcePath,
            request->parameters,
            headers,
            request->content));

    std::string stringToSign = AWS3HttpsSigner::stringToSign(unsignedRequest);
    std::string signature = Signer::sign(algorithm,
                                         credentials->secretAccessKey,
                                         stringToSign);

    std::stringstream xAmznAuthorization;
    xAmznAuthorization << "AWS3-HTTPS AWSAccessKeyId="
                       << credentials->accessKeyId
                       << ",Algorithm="
                       << algorithm
                       << ",Signature="
                       << signature;

    headers.insert(HeaderEntry(
            "X-Amzn-Authorization",
            Maybe<std::string>::just(xAmznAuthorization.str())));

    RequestHandle signedRequest(new Request(
            request->httpMethod,
            request->endpoint,
            request->resourcePath,
            request->parameters,
            headers,
            request->content));

    return signedRequest;
}

AWS3HttpsSigner::AWS3HttpsSigner()
{
}

AWS3HttpsSigner::~AWS3HttpsSigner()
{
}

RequestHandle AWS3HttpsSigner::operator()(CredentialsHandle credentials,
                                          RequestHandle request,
                                          std::time_t time) const
{
    return AWS3HttpsSigner::sign(credentials, request, time);
}

std::string AWS3HttpsSigner::stringToSign(RequestHandle request)
{
    HeaderMap lowerHeaders(Signer::lowerCaseKey(request->headers));

    HeaderMap::const_iterator ptr = lowerHeaders.find("x-amz-date");
    if (ptr != lowerHeaders.end() && ptr->second)
    {
        return *(ptr->second);
    }

    ptr = lowerHeaders.find("date");
    if (ptr != lowerHeaders.end() && ptr->second)
    {
        return *(ptr->second);
    }

    return std::string();
}

}

