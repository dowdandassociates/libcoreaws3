/*
 *
 * libcoreaws3/src/common.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "common.hpp"

#include <cstdlib>
#include <fstream>
#include <istream>
#include <sstream>
#include <stdexcept>

#include <iostream>

std::string requestToCurl(
        coreaws3::SignerHandle signer,
        coreaws3::CredentialsHandle credentials,
        coreaws3::RequestHandle request,
        std::time_t time)
{
    coreaws3::RequestHandle signedRequest =
            (*signer)(credentials, request, time);
    return requestToCurl(signedRequest);
}


std::string requestToCurl(
        coreaws3::SignerHandle signer,
        coreaws3::CredentialsHandle credentials,
        coreaws3::RequestHandle request)
{
    return requestToCurl(signer,
                         credentials,
                         request,
                         coreaws3::DateTimeUtils::now());
}

std::string requestToCurl(
        coreaws3::SignerHandle signer,
        coreaws3::CredentialsProviderHandle credentialsProvider,
        coreaws3::RequestHandle request,
        std::time_t time)
{
    coreaws3::RequestHandle signedRequest =
            (*signer)(credentialsProvider, request, time);
    return requestToCurl(signedRequest);
}


std::string requestToCurl(
        coreaws3::SignerHandle signer,
        coreaws3::CredentialsProviderHandle credentialsProvider,
        coreaws3::RequestHandle request)
{
    return requestToCurl(signer,
                         credentialsProvider,
                         request,
                         coreaws3::DateTimeUtils::now());
}

std::string requestToCurl(coreaws3::RequestHandle request)
{
    return httpRequestToCurl(
            coreaws3::HttpRequestFactory::convertRequest(request));
}

std::string httpRequestToCurl(coreaws3::HttpRequestHandle httpRequest)
{
    std::stringstream strbuf;

    strbuf << "curl -v -L \"" << httpRequest->url << '"';

    switch (httpRequest->method)
    {
    case coreaws3::HttpMethod::GET:
        strbuf << " --get";
        break;
    case coreaws3::HttpMethod::DELETE:
        strbuf << " --request DELETE";
        break;
    case coreaws3::HttpMethod::HEAD:
        strbuf << " --head";
        break;
    case coreaws3::HttpMethod::PUT:
        strbuf << " --request PUT";
        strbuf << " --data-binary \"" << getInput(httpRequest) << '"';
        break;
    case coreaws3::HttpMethod::POST:
        strbuf << " --request POST";
        strbuf << " --data-binary \"" << getInput(httpRequest) << '"';
        break;
    }

    std::map<std::string, std::string>::const_iterator ptr =
            httpRequest->headers.begin();
    std::map<std::string, std::string>::const_iterator end =
            httpRequest->headers.end();
    for (; ptr != end; ++ptr)
    {
        strbuf << " --header \"" << ptr->first;

        if (ptr->second != "")
        {
            strbuf << ": " << ptr->second;
        }
        else if (coreaws3::StringUtils::startsWithIgnoreCase(ptr->first, "x-"))
        {
            strbuf << ';';
        }
        else
        {
            strbuf << ':';
        }
        strbuf << '"';
    }

    return strbuf.str();
}

std::string getInput(coreaws3::HttpRequestHandle httpRequest)
{
    coreaws3::InputStreamHandle inputStream = httpRequest->content;
    std::stringstream buf;
    while (inputStream->good())
    {
        char c = inputStream->get();
        if (inputStream->good())
        {
            if (c == '"')
            {
                buf << "\\\"";
            }
            else if (c == '\\')
            {
                buf << "\\\\";
            }
            else
            {
                buf << c;
            }
        }
    }

    return buf.str();
}

coreaws3::CredentialsHandle readCredentials()
{
    char* awsCredentialFile = getenv("AWS_CREDENTIAL_FILE");
    std::string path;
    if (NULL != awsCredentialFile)
    {
        path = awsCredentialFile;
    }
    else
    {
        path = "./aws_credentials";
    }

    std::ifstream in(path.c_str());
    if ((in.rdstate() & std::ifstream::failbit) != 0)
    {
        throw std::runtime_error("Cannot open credentials file");
    }

    std::string str;
    in >> str;
    int pos = str.find_first_of("=");
    if (pos == std::string::npos || pos == str.length() - 1)
    {
        throw std::runtime_error("No \"=\" on AWSAccessKeyId line, or no value");
    }

    std::string name = str.substr(0, pos);
    if (name != "AWSAccessKeyId")
    {
        throw std::runtime_error("Variable was not AWSAccessKeyId");
    }

    std::string awsAccessKeyId = str.substr(pos + 1);

    in >> str;
    pos = str.find_first_of("=");

    if (pos == std::string::npos || pos == str.length() - 1)
    {
        throw std::runtime_error("No \"=\" on AWSSecretKey line, or no value");
    }

    name = str.substr(0, pos);
    if (name != "AWSSecretKey")
    {
        throw std::runtime_error("Variable was not AWSSecretKey");
    }

    std::string awsSecretKey = str.substr(pos + 1);

    in >> str;
    pos = str.find_first_of("=");
    std::string sessionToken = "";
    if (pos != std::string::npos && pos != str.length() - 1)
    {
        name = str.substr(0, pos);
        if (name == "SessionToken")
        {
            sessionToken = str.substr(pos + 1);
        }
    }

    if (sessionToken != "")
    {
        coreaws3::CredentialsHandle credentials(new coreaws3::Credentials(
                awsAccessKeyId,
                awsSecretKey,
                sessionToken));
        return credentials;
    }
    else
    {
        coreaws3::CredentialsHandle credentials(new coreaws3::Credentials(
                awsAccessKeyId,
                awsSecretKey));
        return credentials;
    }

}

