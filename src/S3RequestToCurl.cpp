/*
 *
 * libcoreaws3/src/S3RequestToCurl.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include <exception>
#include <iostream>
#include <sstream>
#include <string>

#include "common.hpp"

const char* BUCKET = "dowdandassociates-test";
const char* KEY = "test.txt";

int main(int argc, char** argv)
{
    try
    {
        coreaws3::SignerHandle signer(new coreaws3::S3Signer());
        coreaws3::CredentialsProviderHandle credentialsProvider(
                new coreaws3::EnvironmentVariableCredentialsProvider());

        coreaws3::HttpMethod::Type method = coreaws3::HttpMethod::GET;
        coreaws3::EndpointHandle endpoint(new coreaws3::Endpoint(
                coreaws3::Scheme::https,
                "s3.amazonaws.com",
                coreaws3::Maybe<coreaws3::Port>::nothing()));
        std::stringstream strbuf;
        strbuf << '/' << BUCKET << '/' << KEY;
        std::string resourcePath = strbuf.str();
        coreaws3::ParameterMap parameters;
        parameters.insert(coreaws3::ParameterEntry(
                "acl",
                coreaws3::Maybe<std::string>::nothing()));
        coreaws3::HeaderMap headers;
        coreaws3::Maybe<coreaws3::InputStreamHandle>::Type content =
                coreaws3::Maybe<coreaws3::InputStreamHandle>::nothing();

        coreaws3::RequestHandle request(new coreaws3::Request(
                method,
                endpoint,
                resourcePath,
                parameters,
                headers,
                content));

        std::cout << requestToCurl(signer,
                                   credentialsProvider,
                                   request) << std::endl;
    
        return 0;
    }
    catch (std::exception e)
    {
        std::cerr << "Exception: " << e.what() << std::endl;
    }
    catch (...)
    {
        std::cerr << "Unknown exception" << std::endl;
    }

    return 1;
}

